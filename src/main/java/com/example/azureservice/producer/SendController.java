package com.example.azureservice.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.azureservice.model.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@RestController
public class SendController {

    private static final String DESTINATION_NAME = "exampletopic";

    private static final Logger logger = LoggerFactory.getLogger(SendController.class);

    @Autowired
    private JmsTemplate jmsTemplate;
    
	@PostMapping("/msg")
	public ResponseEntity<String> sendMessage(@RequestBody User usr) {
		 logger.info("Sending message");
	        jmsTemplate.convertAndSend(DESTINATION_NAME, new User(usr.getName(),usr.getMsg()));
		return ResponseEntity.ok("Sent! "+usr.getMsg());
	}

    
}
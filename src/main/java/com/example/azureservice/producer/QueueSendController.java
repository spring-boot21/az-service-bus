package com.example.azureservice.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.azureservice.model.User;


@RestController
public class QueueSendController {


	private static final String QUEUE_NAME = "examplequeue";

	private static final Logger LOGGER = LoggerFactory.getLogger(QueueSendController.class);

	@Autowired
	private JmsTemplate jmsTemplate;

	@PostMapping("/queue")
	public ResponseEntity<String> sendMessage(@RequestBody User message) {
		LOGGER.info("Sending message");

		jmsTemplate.convertAndSend(QUEUE_NAME, new User(message.getName(),message.getMsg()), jmsMessage -> {
			jmsMessage.setStringProperty("JMSXGroupID", "xxxeee");
			return jmsMessage;
		});
		return ResponseEntity.ok("Sent! "+message.getMsg());

	}
	
	@PostMapping("/topic")
	public ResponseEntity<String> sendMessageTopic(@RequestBody User message) {
		LOGGER.info("Sending message");

		jmsTemplate.convertAndSend(QUEUE_NAME, new User(message.getName(),message.getMsg()), jmsMessage -> {
			jmsMessage.setStringProperty("JMSXGroupID", "xxxeee");
			return jmsMessage;
		});
		return ResponseEntity.ok("Sent! "+message.getMsg());

	}
	
}
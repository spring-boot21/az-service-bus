package com.example.azureservice.consumer;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import com.example.azureservice.model.User;

@Component
public class TopicReceiveController {

    private static final String TOPIC_NAME = "exampletopic";

    private static final String SUBSCRIPTION_NAME = "examplesubscription";

    private final Logger logger = LoggerFactory.getLogger(TopicReceiveController.class);
    
    @JmsListener(destination = TOPIC_NAME, containerFactory = "topicJmsListenerContainerFactory",
            subscription = SUBSCRIPTION_NAME)
    public void receiveMessageRAj(User user) {
  
    	logger.info("Received message: {}, {}", user.getName(), user.getMsg());
    }
}

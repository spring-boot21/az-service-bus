package com.example.azureservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AzureServiceBusPocApplication {

	public static void main(String[] args) {
		SpringApplication.run(AzureServiceBusPocApplication.class, args);
	}

}

package com.example.azureservice.model;

import java.io.Serializable;

// Define a generic User class.
public class User implements Serializable {

    private static final long serialVersionUID = -295422703255886286L;

    private String name;

    private String msg;
    public User() {
    }

    public User(String name, String msg) {
		super();
		this.name = name;
		this.msg = msg;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	
    @Override
	public String toString() {
		return "User [name=" + name + ", msg=" + msg + "]";
	}

	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
package com.example.azureservice.model;

public class MessageInfo {

	String messageDetails;
	String msgType;
    public String getMessageDetails() {
		return messageDetails;
	}
	public void setMessageDetails(String messageDetails) {
		this.messageDetails = messageDetails;
	}
	public String getMsgType() {
		return msgType;
	}
	public void setMsgType(String msgType) {
		this.msgType = msgType;
	}
	@Override
	public String toString() {
		return "Message [messageDetails=" + messageDetails + ", msgType=" + msgType + "]";
	}
	public MessageInfo(String messageDetails, String msgType) {
		super();
		this.messageDetails = messageDetails;
		this.msgType = msgType;
	}
}
